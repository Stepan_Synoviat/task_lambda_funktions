package com.synoviat;


public class RunTask1 {
	public static void runTask1() {
		MyInterface myInterface = (n1, n2, n3) -> Integer.max(n1, Integer.max(n2, n3));
		System.out.println("Max value is: " + myInterface.myFunction(5, 6, 8));

		MyInterface myInterface1 = (n1, n2, n3)->(n1+n2+n3)/3;
		System.out.println("Average value is "+myInterface1.myFunction(3, 6, 12));
	}
}
