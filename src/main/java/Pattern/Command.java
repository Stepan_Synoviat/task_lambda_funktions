package Pattern;


//the command interface
@FunctionalInterface
public interface Command{
	public void execute();
}
